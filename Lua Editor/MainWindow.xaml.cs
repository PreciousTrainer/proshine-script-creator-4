﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace ProShine_Script_Creator
{
    public partial class MainWindow
    {
        private readonly List<string> _names = new List<string>();
        private readonly List<string> _maps = new List<string>();
        private StringBuilder _codeBuilder;

        public MainWindow()
        {
            InitializeComponent();
            NameList.ItemsSource = _names;
            MapList.ItemsSource = _maps;
            AddButton.Click += AddButton_Click;
            RemoveButton.Click += RemoveButton_Click;
            AddMapButton.Click += AddMapButton_Click;
            RemoveMapButton.Click += RemoveMapButton_Click;
            Init();
        }
        //Remove pokemon from list
        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            if (_names.Count <= 0) return;

            if (NameList.SelectedIndex == -1) return;

            _names.RemoveAt(NameList.SelectedIndex);
            NameList.Items.Refresh();
            NameList.SelectedIndex = NameList.Items.Count - 1;
        }
        //Add pokemon to list
        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            if (_names.Contains(PokeBox.Text)) return;

            _names.Add(PokeBox.Text);
            NameList.Items.Refresh();
            NameList.SelectedIndex = NameList.Items.Count - 1;
            NameList.ScrollIntoView(NameList.SelectedItem);
        }
        //Add map name to list
        private void AddMapButton_Click(object sender, RoutedEventArgs e)
        {
            if (_maps.Contains(MapTextBox.Text) || MapTextBox.Text == string.Empty && _maps.Count >= 1) return;

            if (_maps.Count <= 0)
            {
                _maps.Add(PokeCenterBox.Text);
                MapList.Items.Refresh();
                MapList.SelectedIndex = MapList.Items.Count - 1;
                MapList.ScrollIntoView(MapList.SelectedItem);
            }
            else
            {
                _maps.Add(MapTextBox.Text);
                MapList.Items.Refresh();
                MapList.SelectedIndex = MapList.Items.Count - 1;
                MapList.ScrollIntoView(MapList.SelectedItem);
                MapTextBox.Clear();
            }
        }

        //Remove map name from list
        private void RemoveMapButton_Click(object sender, RoutedEventArgs e)
        {
            if (_maps.Count <= 0) return;

            if (MapList.SelectedIndex == -1) return;

            _maps.RemoveAt(MapList.SelectedIndex);
            MapList.Items.Refresh();
            MapList.SelectedIndex = NameList.Items.Count - 1;
        }

        private void Init()
        {
            OutputWin.AppendText("                   ~ProShine Script Creator~" + Environment.NewLine);
            OutputWin.AppendText("            https://proshine-bot.com/thread-2769.html" + Environment.NewLine);

            foreach (var pokeName in PokemonNames.PokemonNamesArray)
            {
                PokeBox.Items.Add(pokeName);
            }

            foreach (var pokecenter in PokeCenters.PokeCentersArray)
            {
                PokeCenterBox.Items.Add(pokecenter);
            }
        }

        private void Print(string msg)
        {
            OutputWin.AppendText("PSC: " + msg + Environment.NewLine);
            OutputWin.ScrollToEnd();
        }

        private void WriteCode(string code, int indent = 0)
        {
            for (var i = 0; i < indent; i++)
            {
                code = "\t" + code;
            }
            _codeBuilder.AppendLine(code);
        }

        private void OnCheckChanged(object sender, RoutedEventArgs e)
        {
            Print(UsingFalseSwipe.IsChecked == true ? "Using False Swipe" : "Not using Flase Swipe");
        }

        private void CreditsButton_Click(object sender, RoutedEventArgs e)
        {
            Print("Credits Icesythe7, Zonz, PresiousTrainer");
        }

        //Generate the script
        private void CreateScript_Click(object sender, RoutedEventArgs e)
        {
            Print("Creating your custom script, please wait...");

            _codeBuilder = new StringBuilder();

            //for those users who wants to use false swipe
            WriteCode("local falseSwipePokeUser = nil");
            WriteCode("");

            WriteCode("local catchList =");
            WriteCode("{");

            foreach (var name in _names)
            {
                if (name.Contains(" ")) // Mr Mime / Farfetch 'd
                    WriteCode("['" + name + "'] = true,", 1);
                else
                    WriteCode(name + " = true,", 1);
            }
            WriteCode("}");

            WriteCode("");

            //Return pokemon with false swipe
            if (UsingFalseSwipe.IsChecked == true)
            {
                WriteCode("function getPokemonWithMove(moveName)");
                WriteCode("for i = 1, getTeamSize() do", 1);
                WriteCode("for m = 1, 4 do", 2);
                WriteCode("if getPokemonMoveName(i, m) == moveName then", 3);
                WriteCode("return i", 4);
                WriteCode("end", 3);
                WriteCode("end", 2);
                WriteCode("end", 1);
                WriteCode("return nil", 1);
                WriteCode("end");

                WriteCode("");

                WriteCode("function onStart()");
                WriteCode("falseSwipePokeUser = getPokemonWithMove('False Swipe')", 1);
                WriteCode("end");
                WriteCode("");
            }

            //onPathAction()
            if (_maps.Count > 1)
            {
                var lastMap = _maps[_maps.Count - 1]; //user's last map
                var firstMap = _maps[0]; // user's first map               

                WriteCode("function onPathAction()");

                if (UsingFalseSwipe.IsChecked == false)
                {
                    WriteCode("if isPokemonUsable(1) then", 1);
                }
                else
                {
                    WriteCode("falseSwipePokeUser = getPokemonWithMove('False Swipe')", 1);
                    WriteCode("");
                    WriteCode("if isPokemonUsable(1) and getRemainingPowerPoints(falseSwipePokeUser, 'False Swipe') > 1 then", 1);
                }

                WriteCode("if getMapName() == '" + _maps[0] + "' then", 2);
                WriteCode("return moveToMap('" + _maps[1] + "')", 3);

                for (var i = 1; i <= _maps.Count - 1; i++)
                {
                    WriteCode("elseif getMapName() == '" + _maps[i] + "' then", 2);
                    var mapIndexInList = _maps.FindIndex(x => x.StartsWith(_maps[i])); //finding the map index
                    if (_maps[i] != lastMap)
                    {
                        WriteCode("return moveToMap('" + _maps[mapIndexInList + 1] + "')", 3); //adding moveToMap() for go to next map
                    }
                    else
                    {
                        WriteCode("return moveToGrass() or moveToWater() or moveToNormalGround()", 3);
                        WriteCode("end", 2);
                    }
                }

                WriteCode("else", 1);
                WriteCode("if getMapName() == '" + lastMap + "'" + " then", 2);
                WriteCode("return moveToMap('" + _maps[_maps.IndexOf(lastMap) - 1] + "')", 3);
                _maps.Reverse(); //now time to go to Pokecenter for this we reverse the whole Map list 

                for (var i = 1; i <= _maps.Count - 1; i++)
                {
                    WriteCode("elseif getMapName() == '" + _maps[i] + "'" + " then", 2);

                    var mapIndexInList = _maps.FindIndex(x => x.StartsWith(_maps[i]));

                    if (_maps[i] != firstMap)
                    {
                        WriteCode("return moveToMap('" + _maps[mapIndexInList + 1] + "')", 3);
                    }
                    else
                    {
                        WriteCode("return usePokecenter()", 3);
                        WriteCode("end", 2);
                    }
                }

                WriteCode("end", 1);
                WriteCode("end");
                /*reversing the whole map again 
                 * coz if the user want to make another script with same map then the script will have an error
                 */
                _maps.Reverse();
            }
            else
            {
                WriteCode("function onPathAction()");
                WriteCode("return moveToGrass() or moveToWater() or moveToNormalGround()", 1);
                WriteCode("end");
            }

            WriteCode("");

            //onBattleAction()
            WriteCode("function onBattleAction()");
            WriteCode("if isWildBattle() then", 1);
            WriteCode("if isOpponentShiny() or getOpponentForm() ~= 0 or catchList[getOpponentName()] then", 2);

            //if user want to use false swipe then the onBattleAction() will be like this
            if (UsingFalseSwipe.IsChecked == true)
            {
                WriteCode("if getActivePokemonNumber() == 1 then", 3);
                WriteCode("return sendPokemon(falseSwipePokeUser) or run()", 4);
                WriteCode("elseif (getActivePokemonNumber() == falseSwipePokeUser ) and (getOpponentHealth() > 1 ) then", 3);
                WriteCode("return weakAttack() or sendPokemon(3) or sendAnyPokemon() or run()", 4);
                WriteCode("elseif (getOpponentHealth() == 1 ) then", 3);
                WriteCode("return useItem('Pokeball') or useItem('Great Ball') or useItem('Ultra Ball') or sendUsablePokemon()", 4);
                WriteCode("end", 3);
                WriteCode("return useItem('Pokeball') or useItem('Great Ball') or useItem('Ultra Ball') or sendAnyPokemon()", 3);
            }
            else
            {
                WriteCode("return useItem('Pokeball') or useItem('Great Ball') or useItem('Ultra Ball') or sendAnyPokemon()", 3);
            }

            WriteCode("else", 2);
            WriteCode("return run() or attack() or sendUsablePokemon() or sendAnyPokemon()", 3);
            WriteCode("end", 2);
            WriteCode("else", 1);
            WriteCode("return attack() or sendUsablePokemon() or sendAnyPokemon()", 2);
            WriteCode("end", 1);
            WriteCode("end");

            //Copying the script
            System.Windows.Forms.Clipboard.SetText(_codeBuilder.ToString());

            Print("Script generated and copied to your clipboard.");
            

        }
    }
}
